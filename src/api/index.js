import { API_METHODS, API_KEY, HASH, TS } from "../config";
const axios = require('axios');

export const Network = async (method, url, postData) => {
  let options = {};
  if (method === API_METHODS.GET) {
    options = {
      method: method,
    };
  } else {
    options = {
      method: method,
      body: JSON.stringify(postData),
    };
  }
  let completeUrl = url + API_KEY + HASH + TS;
  try {
    const response = await axios.get(completeUrl);
    return response.data.data.results;
    // console.log(response);
    // return data;
  } catch (error) {
    console.error(error);
  }
//   try {
//     const response = await fetch(completeUrl, options);
//     debugger;
//     const data = response.json();
//     return data;
//   } catch (e) {
//     throw new Error(e);
//   }
};

import React from "react";
import '../styles.css';

const ComicCard = (props) => {
  const {  name, resourceURI } = props.data;
  const render = () => {
    return (
      <div className="comicCard">
          <p>{name}</p>
      </div>
    );
  };
  return render();
};

export default ComicCard;

import React from "react";
import '../styles.css';
import ComicCard from "./comicCard";
const Card = (props) => {
  const { name, description, thumbnail, comics } = props.data;

  const renderComicList = () => {
      if(comics.items.length > 0){
        return comics.items.map((item, index)=>{
            return <ComicCard data={item} />
        })  
      }
      else{
          return(
              <div className="loading">
                  No Data
              </div>
          )
      }
  }
  const render = () => {
    return (
      <div className="element">
        <figure>
          <img
            alt="name"
            className="image"
            src={thumbnail.path + "." + thumbnail.extension}
          />
          <p>{name}</p>
          <figcaption>{description === "" ? "N/A" : description}</figcaption>
        </figure>
        <div className="comicCardContainer">
            {renderComicList()}
        </div>
      </div>
    );
  };
  return render();
};

export default Card;

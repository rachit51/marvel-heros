import React, { useEffect, useState, useCallback } from "react";
import "./styles.css";
import { Network } from "../../api";
import { API_METHODS, BASE_URL } from "../../config";
import Card from './subView/card';
const Hero = () => {
  const [listData, setListData] = useState([]);
  const [keywords, setKeywords] = useState('');
  useEffect(() => {
    apiCall();
  }, []);
  useEffect(() => {
    apiCall();
  }, [keywords]);
  const apiCall = async () => {
      if(keywords !== ""){
        setListData(await Network(API_METHODS.GET, BASE_URL+`nameStartsWith=${keywords}&` , {}));
      }
      else{
        setListData(await Network(API_METHODS.GET, BASE_URL, {}));
      }
  };

  const onKeywordsChange = (e) =>{
    setKeywords(e.target.value);
    e.preventDefault();
  }

  const renderList = useCallback(() => {
      if(listData !== undefined){
        if (listData.length > 0) {
          return listData.map((item,index)=>{
              return <Card key={index} data={item} />
          })
        } else {
          return (
            <div className="loading">
              <p>No Data</p>
            </div>
          );
        }
      }
      else{
        return (
            <div className="loading">
              <p>Loading</p>
            </div>
          ); 
      }

  }, [listData]);
  const render = () => {
    return (
      <div className="container">
        <h2>Marvel Heros</h2>
        <div>
            <input placeholder="Enter keywords" type="search" onChange={(e)=>onKeywordsChange(e)}/>
        </div>
        <div className="parent">
            {renderList()}
            </div>
      </div>
    );
  };

  return render();
};

export default Hero;

export const BASE_URL = "https://gateway.marvel.com/v1/public/characters?";
export const API_KEY = "apikey=f1d1e4d3be4e3c1d8118df02cbbc39c5";
export const HASH = "&hash=b9db91fd5c70ea17c453aeb33e41fea8";
export const TS = "&ts=1";
export const API_METHODS = {
  GET: "GET",
  POST: "POST",
};
